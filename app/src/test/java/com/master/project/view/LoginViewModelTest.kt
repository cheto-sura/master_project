package com.master.project.view

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import com.master.project.base.BaseUTTest
import com.master.project.data.rest.repository.GeneralRepository
import com.master.project.di.module.networkModule
import com.master.project.di.module.repositoryModule
import com.master.project.di.module.utilityModule
import com.master.project.di.module.viewModelModule
import com.master.project.vo.model.body.BodyLogin
import com.master.project.vo.model.response.ResponseLogin
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.Matchers.`is`
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.inject
import org.mockito.Mockito.`when`
import org.robolectric.res.android.Asset


@RunWith(JUnit4::class)
class LoginViewModelTest: BaseUTTest() {

    private val generalRepository: GeneralRepository  by inject()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private var userName = "gobank@gmail.com"
    private var password = "password"
    private var token = "7C57196B27D826A2165F382821CF37C57196B27D826A2165F382821CF3"
    private var language = "th"

    @Before
    fun start(){
        super.setUp()
        startKoin {
            modules(arrayListOf(networkModule, utilityModule, repositoryModule, viewModelModule))
        }
    }

    @Test
    fun testLoginSuccess_returnResponse() =  runBlocking {
        //mockNetworkResponseWithFileContent("objectLogin.json", HttpURLConnection.HTTP_OK)

        val dataReceived = generalRepository.onLogin(BodyLogin(userName, password,token,language))

        val testObserver = TestObserver<ResponseLogin>()
        dataReceived.subscribe(testObserver)

        assertThat(testObserver.values()[0].data, `is`(notNullValue()))
    }

    @Test
    fun testLoginCheckName_returnResponseFullName() =  runBlocking {
        //mockNetworkResponseWithFileContent("objectLogin.json", HttpURLConnection.HTTP_OK)

        val dataReceived = generalRepository.onLogin(BodyLogin(userName,
            password,token,language))
        val testObserver = TestObserver<ResponseLogin>()
        dataReceived.subscribe(testObserver)

        assertThat(testObserver.values()[0].data.fullname, `is`("Vittavach"))
    }

    @Test
    fun testLoginError_returnNoResponse() =  runBlocking {
        //mockNetworkResponseWithFileContent("objectLogin.json", HttpURLConnection.HTTP_OK)

        val dataReceived = generalRepository.onLogin(BodyLogin("gobank2@gmail.com",
            "password","","th"))
        val testObserver = TestObserver<ResponseLogin>()
        dataReceived.subscribe(testObserver)

        assertThat(testObserver.values().isEmpty(), `is`(true))
    }

    @After
    override fun tearDown() {
        stopKoin()
    }

}