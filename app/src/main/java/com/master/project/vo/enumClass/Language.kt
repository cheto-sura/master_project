package com.master.project.vo.enumClass

enum class Language {
    TH,
    EN
}
