package com.master.project.vo.enumClass

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
