package com.master.project.utils.rxBus

object EventRxBus {

    fun <RequestType> onAddEventRxBus(rxClass: RequestType) {
        RxBus.publish(rxClass)
    }
}
