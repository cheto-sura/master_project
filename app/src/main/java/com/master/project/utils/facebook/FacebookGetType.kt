package com.master.project.utils.facebook

enum class FacebookGetType {
    LOGIN,
    REGISTER
}
