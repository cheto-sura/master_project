package com.master.project.utils

import android.content.Context
import android.content.Intent
import com.master.project.utils.rxBus.RxBus
import com.master.project.view.login.LoginActivity
import com.master.project.vo.RxEvent
import io.reactivex.disposables.Disposable

class TokenExpired constructor(val context: Context) {

    private var tokenExpiredDisposable: Disposable? = null

    fun doCheckTokenExpire() {
        tokenExpiredDisposable = RxBus.listen(RxEvent::class.java).subscribe {
            val intent = Intent(context, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }

    fun onDestroyDisposable() {
        tokenExpiredDisposable?.let {
            if (!it.isDisposed) it.dispose()
        }
    }
}
