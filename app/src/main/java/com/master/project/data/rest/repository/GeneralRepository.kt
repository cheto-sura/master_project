package com.master.project.data.rest.repository

import com.master.project.data.rest.APIService
import com.master.project.utils.ConvertRequestBody.onConvertFileToMultipartBody
import com.master.project.utils.ConvertRequestBody.onConvertObjectToMap
import com.master.project.vo.model.body.BodyLogin
import com.master.project.vo.model.body.BodyRegister
import com.master.project.vo.model.response.ResponseLogin
import io.reactivex.Observable
import java.io.File

class GeneralRepository
constructor(
    private val apiService: APIService
) {

    fun onLogin(paramLogin: BodyLogin): Observable<ResponseLogin> = apiService.doLogin(paramLogin)

    fun onRegister(data: BodyRegister, profileImage: File?) =
        apiService.registerUser(
            "th",
            onConvertFileToMultipartBody(profileImage!!, "profileImage"),
            onConvertObjectToMap(data)
        )

    fun getOrderList(language: String, pageCurrent: Int, token: String) =
        apiService.getOrderBookings(language, token, pageCurrent)
}
