package com.master.project.di.module

import com.master.project.view.base.ToolbarViewModel
import com.master.project.view.login.LoginViewModel
import com.master.project.view.main.MainViewModel
import com.master.project.view.register.RegisterViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { ToolbarViewModel() }

    viewModel { LoginViewModel(get()) }

    viewModel { RegisterViewModel(get()) }

    viewModel { MainViewModel(get()) }
}