package com.master.project.di.module


import com.master.project.data.rest.APIService
import com.master.project.data.rest.OkHttpClientBuilder
import com.master.project.data.rest.RetrofitBuilder
import org.koin.dsl.module

val networkModule = module {

    single { RetrofitBuilder }

    single<APIService> { get<RetrofitBuilder>().build(
        OkHttpClientBuilder.getUrlServer()) }
}
