package com.master.project.di.module

import com.master.project.data.rest.repository.GeneralRepository
import org.koin.dsl.module

val repositoryModule = module {

    single { GeneralRepository(get()) }

}