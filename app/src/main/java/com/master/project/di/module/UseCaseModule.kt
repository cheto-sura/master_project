package com.master.project.di.module

import com.master.project.domain.GeneralUseCase
import org.koin.dsl.module

val useCaseModule = module {

    single { GeneralUseCase(get(), get()) }

}