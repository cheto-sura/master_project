package com.master.project.di.module

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.master.project.data.local.Preferences
import com.master.project.utils.CheckPermission
import com.master.project.utils.TokenExpired
import com.master.project.utils.Utils
import com.master.project.utils.dialog.DialogPresenter
import com.master.project.utils.googleMap.MapUtils
import com.master.project.utils.imageManagement.ConvertUriToFile
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val utilityModule = module {

    single { Preferences(androidApplication()) }

    factory { (activity: FragmentActivity) -> DialogPresenter(activity) }

    single { MapUtils(androidApplication()) }

    single { TokenExpired(androidApplication()) }

    single { Utils(androidApplication(), get()) }

    factory { (activity: AppCompatActivity) ->
        CheckPermission(activity, ConvertUriToFile(androidApplication()))
    }
}


