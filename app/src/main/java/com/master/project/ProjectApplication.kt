package com.master.project

import android.app.Application
import androidx.multidex.MultiDex
import com.master.project.di.module.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class ProjectApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        MultiDex.install(this)

        startKoin {
            androidContext(this@ProjectApplication)
            modules(
                arrayListOf(
                    networkModule, utilityModule, repositoryModule,
                    useCaseModule, viewModelModule
                )
            )
            androidLogger()
        }
    }
}
