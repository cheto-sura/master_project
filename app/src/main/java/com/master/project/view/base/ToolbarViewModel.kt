package com.master.project.view.base

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.master.project.utils.SingleLiveData

class ToolbarViewModel : ViewModel() {

    val onClickToolbar = SingleLiveData<String>()

    val titleToolbarView = ObservableField("")

    fun onClickBack() {
        onClickToolbar.value = "intentBack"
    }
}
