package com.master.project.view.login

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.master.project.domain.GeneralUseCase
import com.master.project.utils.SingleLiveData
import com.master.project.utils.TextHelper
import com.master.project.utils.watcher.TextWatcherAdapter
import com.master.project.vo.model.body.BodyLogin

class LoginViewModel (private val generalUseCase: GeneralUseCase) : ViewModel() {

    val etUserName = ObservableField("test@gmail.com")

    val etPassWord = ObservableField("password")

    val isStatusButtonClick = ObservableField(false)

    var mLoginCall = SingleLiveData<BodyLogin>()

    var mOnClickListener = SingleLiveData<String>()

    val onUserNameTextChanged = TextWatcherAdapter { s ->
        etUserName.set(s)
        checkEventButtonClick()
    }

    val onPasswordTextChanged = TextWatcherAdapter { s ->
        etPassWord.set(s)
        checkEventButtonClick()
    }

    fun onCLickTest(){
        Log.i("test","onClick")
    }

    fun onClickLogin() {
        mLoginCall.value = BodyLogin( etUserName.get()!!, etPassWord.get()!!,
            "7C57196B27D826A2165F382821CF37C57196B27D826A2165F382821CF3", "th")
    }

    fun onClickRegister() {
        mOnClickListener.value = "intentRegister"
    }

    val mResponseLogin = Transformations.switchMap(mLoginCall) {
        generalUseCase.doLogin(it)
    }

    fun checkEventButtonClick() {
        isStatusButtonClick.set(TextHelper.isNotEmptyStrings(etUserName.get()) && TextHelper.isNotEmptyStrings(etPassWord.get())
        )
    }
}
